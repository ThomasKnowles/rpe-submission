﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireballs : MonoBehaviour {

	/// <summary>
	/// Bind the fireball sequence
	/// </summary>
	void Awake(){
		EventManager.AddListener ("FireballSequence", FireballSequence);
	}

	/// <summary>
	/// Starts the fireball sequence
	/// </summary>
	void FireballSequence(){
		StartCoroutine ("FireballSequenceRoutine");
	}

	/// <summary>
	/// Routine to run the fireball sequence
	/// </summary>
	/// <returns>The sequence routine.</returns>
	IEnumerator FireballSequenceRoutine(){
		yield return null;
	}
}

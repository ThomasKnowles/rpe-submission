﻿using UnityEngine;

public class Player : MonoBehaviour {
	[SerializeField] private GameObject CameraObject;   // Target for interpolation

    /// <summary>
    /// Interpolate the position, and update rotation
    /// </summary>
    void FixedUpdate(){
        if (!MissileCommand.missileActive)
        {
            transform.rotation = CameraObject.transform.rotation;
            transform.position = Vector3.Lerp(transform.position, CameraObject.transform.position, 0.7f);
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// * This class is attached to a gameobject 
/// * in an initialisation or menu scene, 
/// * and simply adds the Reset method to
/// * the scene manager unload scene delegate
/// </summary>
public class EventReloader : MonoBehaviour {

    public static bool _added = false;

    /// <summary>
    /// Binds the scene unload to the event reset
    /// </summary>
    private void Awake()
    {
        if (!_added)
        {
            SceneManager.sceneUnloaded += EventManager.ResetEvents;
            _added = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System;

public static class EventManager {
	// Dictionary to hold events
	private static Dictionary<string, Action> Events = new Dictionary<string, Action>();

	/// <summary>
	/// Adds event to Event listener
	/// </summary>
	/// <param name="_method">Method.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static void AddListener(string _handler, Action _method){
		if (!EventExists (_handler))
			CreateEvent (_handler);

		Events [_handler] += _method;
	}

    /// <summary>
    /// Removes a Listener from the Events List
    /// </summary>
    /// <param name="_handler"></param>
    /// <param name="_method"></param>
    public static void RemoveListener(string _handler, Action _method)
    {
        if (!EventExists(_handler))
            Events[_handler] -= _method;
    }

	/// <summary>
	/// Calls input event
	/// </summary>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static void RaiseEvent(string _handler){
		if (EventExists (_handler))
			Events [_handler].Invoke ();
		else
			throw new KeyNotFoundException ("No such method exists");
	}

	/// <summary>
	/// Checks if an event exists
	/// </summary>
	/// <returns><c>true</c>, if exists was evented, <c>false</c> otherwise.</returns>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	private static bool EventExists(string _handler){
		return Events.ContainsKey (_handler);
	}

	/// <summary>
	/// Creates the event.
	/// </summary>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	private static void CreateEvent(string _handler){
		Events.Add(_handler, null);
	}

    /// <summary>
    /// Resets the Events table
    /// </summary>
    public static void ResetEvents(UnityEngine.SceneManagement.Scene current)
    {
        Events.Clear();
    }
}


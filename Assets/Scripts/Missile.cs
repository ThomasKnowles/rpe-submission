﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour {
    [SerializeField] private UnityStandardAssets.Characters.FirstPerson.MouseLook mouseLook;    // Handles rotations
    [SerializeField] private GameObject explosionPrefab;                                        // Prefab for explosion
    private Transform _camera;                                                                  // Camera to look throuh
    private Rigidbody rbody;
    private bool initialised = false;                                                           // Can we fly
    private bool exploding = false;                                                             // Have we already exploded

    /// <summary>
    /// Instantiate needed variables to initialise
    /// </summary>
    private void Awake()
    {
        _camera = GameObject.FindGameObjectWithTag("MissileCamera").transform;
        mouseLook.Init(transform, _camera.transform);
        rbody = GetComponent<Rigidbody>();
    }

    /// <summary>
    /// Bind the missile camera, transition and start countdown to flight
    /// </summary>
    private void Start()
    {
        CameraTranistions.instance.BindMissileCamera(transform);
        CameraTranistions.instance.TranistionCamera(CameraTranistions.CameraStates.MISSILE);
        StartCoroutine("CountDown");
    }

    /// <summary>
    /// Update rotation and move forward
    /// </summary>
    private void Update()
    {
        if (initialised)
            transform.position += _camera.forward * Time.deltaTime * 10;

        mouseLook.LookRotation(transform, _camera);
        mouseLook.UpdateCursorLock();
    }

    /// <summary>
    /// Routine to delay flight until player realises movement
    /// </summary>
    /// <returns></returns>
    private IEnumerator CountDown()
    {
        yield return new WaitForSeconds(3f);
        initialised = true;
    }

    /// <summary>
    /// When we collide we need to explode
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        if(!exploding)
            StartCoroutine("ExplosionDelay");
    }

    /// <summary>
    /// Routine to delay explosion so you can see it, and move camera back to player
    /// </summary>
    /// <returns></returns>
    private IEnumerator ExplosionDelay()
    {
        exploding = true;
        CameraTranistions.instance.TranistionCamera(CameraTranistions.CameraStates.MISSILE_EXPLOSION);
        yield return new WaitForSeconds(1f);
        Explode();
        yield return new WaitForSeconds(3f);
        CameraTranistions.instance.TranistionCamera(CameraTranistions.CameraStates.PLAYER);
    }

    /// <summary>
    /// Handles all logic to explode and destroy this object
    /// </summary>
    private void Explode()
    {
        Instantiate(explosionPrefab, transform.position, transform.rotation);
        CameraTranistions.instance.UnBindMissileCamera();
        MissileCommand.missileActive = false;
        Destroy(gameObject, 5f);
    }

}

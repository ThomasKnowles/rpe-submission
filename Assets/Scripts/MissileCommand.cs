﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileCommand : MonoBehaviour {
    [SerializeField] private GameObject MissilePrefab;
    [SerializeField] private GameObject SpawnPoint;

    public static bool missileActive = false;

    /// <summary>
    /// Bind spawning event
    /// </summary>
    private void Awake()
    {
        EventManager.AddListener("Missile", StartMissile);
    }

    private void StartMissile()
    {
        //Spawn missile
        Instantiate(MissilePrefab, SpawnPoint.transform);
        // Set missile active
        missileActive = true;
    }
}

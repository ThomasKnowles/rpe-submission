﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceDoor : MonoBehaviour {

	[SerializeField] private Animator doorAnimator;
	[SerializeField] private GameObject forceField;

    private bool _isAnimating = false;

	void Awake(){
		EventManager.AddListener ("DoorSequence", DoorSequence);
	}

	/// <summary>
	/// Calls the sequence
	/// </summary>
	private void DoorSequence(){
        if(!_isAnimating)
		    StartCoroutine ("DoorSequenceRoutine");
	}

	/// <summary>
	/// Sequence to open the door and suck boxes out
	/// </summary>
	/// <returns>The sequence routine.</returns>
	private IEnumerator DoorSequenceRoutine(){
        _isAnimating = true;

		// Open the door
		doorAnimator.SetTrigger ("OpenDoor");

        // Start the alarm
        EventManager.RaiseEvent("StartAlarm");
		// Delay for 5 seconds
		yield return new WaitForSeconds (5f);
        // End the Alarm
        EventManager.RaiseEvent("EndAlarm");

		// Disable to forcefield
		forceField.SetActive (false);
		// Suck the boxes out
		EventManager.RaiseEvent ("SuckBoxesIntoSpace");


        // Delay for 5 seconds
        yield return new WaitForSeconds (5f);

        // Start the alarm
        EventManager.RaiseEvent("StartAlarm");
        // Reactivate the forcefield
        forceField.SetActive (true);
		// Close the Door
		doorAnimator.SetTrigger ("CloseDoor");
        yield return new WaitForSeconds(3f);
        // End the Alarm
        EventManager.RaiseEvent("EndAlarm");

        _isAnimating = false;
	}
}

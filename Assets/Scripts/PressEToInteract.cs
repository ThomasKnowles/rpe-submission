﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressEToInteract : MonoBehaviour {

	[SerializeField] private float interactRadius;
	[SerializeField] private float fadeTime;
	[SerializeField] private CanvasGroup interactCanvasGroup;

	private InteractableObject obj;
	private GameObject player;



	/// <summary>
	/// Initialise player and obj
	/// </summary>
	void Start () {
		// Find the player
		player = GameObject.FindWithTag("Player");

		obj = GetComponent<InteractableObject> ();
	}
	
	/// <summary>
	/// Check if close enough, check if interacted
	/// </summary>
	void Update () {
		// If the player is in the radius
		if (IsInPlayerRadius()) {

			DisplayInteractPrompt ();
			
			// Accept user input
			if (Input.GetKeyDown("e")) {
				// Call function
				obj.InteractAction();
            }

		} else {
			// Hide the prompt
			HideInteractPrompt();
        }
	}

	/// <summary>
	/// Is the player in the interact radius of this object?
	/// </summary>
	///
	/// <returns>True if in player radius, False otherwise.</returns>
	private bool IsInPlayerRadius () {
		// Find the distance 
		float distance = Vector3.Distance(player.transform.position, transform.position);

		return distance < interactRadius;
	}

	/// <summary>
	/// Fade in the interact prompt
	/// </summary>
	private void DisplayInteractPrompt () {
		// Fade in prompt
		if (interactCanvasGroup.alpha < 1.0f) {
			interactCanvasGroup.alpha += fadeTime * Time.deltaTime;
		} else {
			interactCanvasGroup.alpha = 1.0f;
		}
	}

	/// <summary>
	/// Fade out the interact prompt
	/// </summary>
	private void HideInteractPrompt () {
		// Fade in prompt
		if (interactCanvasGroup.alpha > 0.0f) {
			interactCanvasGroup.alpha -= fadeTime * Time.deltaTime;
		} else {
			interactCanvasGroup.alpha = 0.0f;
		}
	}


}

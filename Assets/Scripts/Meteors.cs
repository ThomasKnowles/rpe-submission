﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteors : MonoBehaviour {
    [SerializeField] private GameObject MeteorSwarmPrefab;
    [SerializeField] private int maxSpawns;
    [SerializeField] private int timer;

    private int curSpawns = 0;

    /// <summary>
    /// Bind the event listener
    /// </summary>
    private void Awake()
    {
        EventManager.AddListener("MeteorSwarm", MeteorSwarm);
    }

    /// <summary>
    /// Spawn a metor swarm, and start the timer
    /// </summary>
    private void MeteorSwarm()
    {
        if (curSpawns <= maxSpawns)
        {
            Instantiate(MeteorSwarmPrefab, transform);
            StartCoroutine("SpawnTimer");
        }
    }

    /// <summary>
    /// Increment the current amount of swarms spawned, wait for timer seconds, decrement again
    /// Locks the amount of current metors to avoid destroying framerate
    /// </summary>
    /// <returns></returns>
    private IEnumerator SpawnTimer()
    {
        curSpawns++;
        yield return new WaitForSeconds(timer);
        curSpawns--;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alarm : MonoBehaviour {
    [SerializeField] private GameObject alarmSystem;
    [SerializeField] private float alarmSpeed;

    /// <summary>
    /// Bind Alarm to event
    /// </summary>
    private void Awake()
    {
        EventManager.AddListener("StartAlarm", StartAlarm);
        EventManager.AddListener("EndAlarm", EndAlarm);
    }

    /// <summary>
    /// Turn Alarm On
    /// </summary>
    private void StartAlarm()
    {
        alarmSystem.SetActive(true);
        StartCoroutine("RotateAlarm");
    }

    /// <summary>
    /// Turn Alarm Off
    /// </summary>
    private void EndAlarm()
    {
        alarmSystem.SetActive(false);
        StopCoroutine("RotateAlarm");
    }

    /// <summary>
    /// Start Rotating the alarm
    /// </summary>
    /// <returns></returns>
    private IEnumerator RotateAlarm()
    {
        while (true)
        {
            transform.Rotate(new Vector3(0f, alarmSpeed * Time.deltaTime, 0f));
            yield return null;
        }
    }

}

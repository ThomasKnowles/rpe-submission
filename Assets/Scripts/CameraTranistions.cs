﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraTranistions : MonoBehaviour {

    public static CameraTranistions instance = null;

    GameObject player;

    /// <summary>
    /// All the cameras in the scene 
    /// </summary>
	public enum CameraStates { PLAYER, MISSILE, MISSILE_EXPLOSION};

	CinemachineBrain brain = new CinemachineBrain();

    public CameraStates currentCamera = CameraStates.PLAYER;

    [Tooltip("Virtual Cameras, Order: PLAYER, MISSILE, MISSILE_EXPLOSION")]
    public CinemachineVirtualCamera[] cameras = new CinemachineVirtualCamera[3];
    

	/// <summary>
    /// Singleton pattern to store instance
    /// </summary>
	void Awake () {

        if (instance == null)
            instance = this;
        else
            Destroy(this);

		brain = Camera.main.GetComponent<CinemachineBrain>();
	}

    /// <summary>
    /// When the missile spawns it binds to the camera slot
    /// </summary>
    /// <param name="_camera"></param>
    public void BindMissileCamera(Transform t)
    {
        cameras[1].transform.position = t.position;
        cameras[1].transform.rotation = t.rotation;
        cameras[1].transform.SetParent(t);
    }

    /// <summary>
    /// When the missile explodes, unbinds the camera
    /// </summary>
    /// <param name="t"></param>
    public void UnBindMissileCamera()
    {
        cameras[1].transform.SetParent(transform);
    }

    /// <summary>
    /// Changes the priority of the cameras
    /// </summary>
    /// <param name="_states"></param>
	public void TranistionCamera(CameraStates _states){
        Debug.Log(_states.ToString());
        cameras[(int)currentCamera].Priority = 0;
        currentCamera = _states;
        cameras[(int)currentCamera].Priority = 10;
    }

}

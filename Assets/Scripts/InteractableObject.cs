﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObject : MonoBehaviour {

	/// <summary>
	/// Action to perform on interact
	/// 
	/// </summary>
	public virtual void InteractAction () {
		Debug.Log("No interaction defined");
	}
}

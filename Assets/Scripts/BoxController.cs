﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BoxController : MonoBehaviour {
	[SerializeField] private List<GameObject> boxes = new List<GameObject> ();
	[SerializeField] private GameObject boxesPrefab;


	/// <summary>
	/// Start this instance, set boxes list
	/// </summary>
	void Start(){
		GameObject[] _boxes = GameObject.FindGameObjectsWithTag ("Box");

		boxes = new List<GameObject> (_boxes);

		EventManager.AddListener ("SuckBoxesIntoSpace", SuckIntoSpace);
		EventManager.AddListener ("ResetBoxes", ReloadBoxes);
	}

	/// <summary>
	/// Respawns boxes
	/// </summary>
	void ReloadBoxes(){
        Destroy(transform.GetChild(0).gameObject);

		// Clear boxes lsit
		boxes.Clear ();

		// Spawn new boxes
		GameObject newBoxes = Instantiate (boxesPrefab,  gameObject.transform);

		foreach (Transform t in newBoxes.transform)
        {
            boxes.Add(t.gameObject);
        }

	}

	/// <summary>
	/// Sucks the boxes into space.
	/// </summary>
	void SuckIntoSpace(){

		// Loops through each of the boxes
		foreach (GameObject b in boxes) {

			// Setup force to suck out box
			Vector3 v = Vector3.forward;
			v.x += Random.Range (-0.2f, 0.2f);
			v.y += Random.Range (-0.2f, 0.2f);
			v *= 5000;

			// Add force to box
			Rigidbody r = b.GetComponent<Rigidbody>();
			r.AddForce (v);
            r.useGravity = false;

			Destroy (b, 5f);

		}

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : InteractableObject {
	[SerializeField] string _event;

	public override void InteractAction () {
		EventManager.RaiseEvent (_event);
	}
}
